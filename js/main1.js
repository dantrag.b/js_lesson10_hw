//////////////////////////////////////////////// Stop Watch ////////////////////////////////////////////////

const [...timerDigits] = document.querySelectorAll('.stopwatch-display > span');
const [...timerButtons] = document.querySelectorAll('.stopwatch-control > button');
const mainContainer = document.querySelector('.container-stopwatch');
const body = document.querySelector('body');


let timerCounter = [0, 0, 0],
  timerInterval,
  currentClassList = 'black';

const stopWatchCouner = () => {
  if (timerCounter[2] < 59) {
    ++timerCounter[2];
    timerCounter[2] < 10 ? timerDigits[2].innerText = `0${timerCounter[2]}`
      : timerDigits[2].innerText = `${timerCounter[2]}`;
  }
  else if (timerCounter[1] < 59) {
    timerCounter[2] = 0;
    timerDigits[2].innerText = `00`;
    ++timerCounter[1];
    timerCounter[1] < 10 ? timerDigits[1].innerText = `0${timerCounter[1]}`
      : timerDigits[1].innerText = `${timerCounter[1]}`;
  }
  else {
    timerCounter[2] = 0;
    timerDigits[2].innerText = `00`;
    timerCounter[1] = 0;
    timerDigits[1].innerText = `00`;
    ++timerCounter[0];
    timerCounter[0] < 10 ? timerDigits[0].innerText = `0${timerCounter[0]}`
      : timerDigits[1].innerText = `${timerCounter[0]}`;
  }
}

const startButton = timerButtons[0].onclick = () => {
  if (typeof timerInterval === 'number') { return }
  timerInterval = setInterval(stopWatchCouner, 1000);
  mainContainer.classList.replace(currentClassList, 'green');
  currentClassList = 'green';
}


const stopButton = timerButtons[1].onclick = () => {
  clearInterval(timerInterval);
  mainContainer.classList.replace(currentClassList, 'red');
  currentClassList = 'red';
  timerInterval = undefined;
}

const resetButton = timerButtons[2].onclick = () => {
  clearInterval(timerInterval);
  mainContainer.classList.replace(currentClassList, 'silver');
  currentClassList = 'silver';
  timerCounter[2] = 0;
  timerDigits[2].innerText = `00`;
  timerCounter[1] = 0;
  timerDigits[1].innerText = `00`;
  timerCounter[0] = 0;
  timerDigits[0].innerText = `00`;
  timerInterval = undefined;
}


//////////////////////////////////////////////// Phone Number Field ////////////////////////////////////////////////

const newDiv = document.createElement('div');
newDiv.classList.add('number__container');
body.append(newDiv);
const myInput = document.createElement('input');
myInput.placeholder = '067-777-77-77';

const myButton = document.createElement('button');
myButton.innerText = 'Save';
newDiv.append(myInput);
newDiv.append(myButton);


myButton.onclick = () => {
  const inputV = document.querySelector('.number__container > input');
  pattern = /^0\d{2}-\d{3}-\d{2}-\d{2}$/;
  if (pattern.test(inputV.value)) {
    inputV.classList.add('green');
    setTimeout(() => {
      document.location.href = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
    }, 3000);
    if (document.querySelector('.errordiv')) {
      const elem = document.querySelector('.errordiv');
      elem.remove();
    }
  }
  else {
    if (!document.querySelector('.errordiv')) {
      let errDiv = document.createElement("div");
      errDiv.classList.add('errordiv');
      errDiv.innerText = 'error';
      inputV.before(errDiv);
      console.log('error');
    }
  }
}

//////////////////////////////////////////////// Slider ////////////////////////////////////////////////


const sliderDiv = document.createElement('div');
sliderDiv.classList.add('slider__container');
body.append(sliderDiv);
const sliderImg = document.createElement('img');
sliderDiv.append(sliderImg);

const images = [
  'https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
  'https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
  'https://naukatv.ru/upload/files/shutterstock_418733752.jpg',
  'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg'
];

let imgNum = 0;

const nextImg = () => {
  if (imgNum === (images.length)) { imgNum = 0; }
  sliderImg.src = images[imgNum];
  imgNum++;
}

setInterval(nextImg, 1000);